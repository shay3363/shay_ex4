#include "FileStream.h"
FileStream::FileStream(const char* path)
{
	this->_myFile = fopen(path, "a");
}
FileStream::~FileStream()
{
	fclose(this->_myFile);
}