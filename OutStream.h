#pragma once
#include <stdio.h>
class OutStream
{
protected:
	FILE* _myFile;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE * outputFile));
};

void endline(FILE * outputFile);