#include "OutStream.h"


OutStream::OutStream()
{
	this->_myFile = stdout;
}

OutStream::~OutStream()
{

}

OutStream& OutStream::operator <<(const char *str)
{
	fprintf(this->_myFile,"%s", str);
	return *this;
}

OutStream& OutStream::operator <<(int num)
{
	fprintf(this->_myFile, "%d", num);
	return *this;
}

OutStream& OutStream::operator <<(void(*pf)(FILE * outputFile))
{
	pf(this->_myFile);
	return *this;
}


void endline(FILE * outputFile)
{
	fprintf(outputFile,"\n");
}
