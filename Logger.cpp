#include "Logger.h"
Logger::Logger()
{
	this->_startLine = false;
	this->os = OutStream();
}
Logger::~Logger()
{

}
int Logger::setStartLine()
{
	static unsigned int logPrinted = 0;
	logPrinted++;
	if (!(this->_startLine))
	{
		this->os << "\n";
		this->_startLine = true;
	}
	return logPrinted;
}
Logger& operator<<(Logger& l, const char *msg)
{
	int timesPrinted = l.setStartLine();
	l.os << "LOG";
	l.os << timesPrinted;
	l.os << "\n";
	l.os << msg;
	l._startLine = false;
	return l;
}
Logger& operator<<(Logger& l, int num)
{
	int timesPrinted = l.setStartLine();
	l.os << "LOG";
	l.os << timesPrinted;
	l.os << "\n";
	l.os << num;
	l._startLine = false;
	return l;
}
Logger& operator<<(Logger& l, void(*pf)(FILE * myFile))
{
	int timesPrinted = l.setStartLine();
	l.os << "LOG";
	l.os << timesPrinted;
	l.os << "\n";
	pf(stdout);
	l._startLine = false;
	return l;
}