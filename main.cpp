#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
int main(int argc, char **argv)
{
	OutStream printer = OutStream();
	printer << "I am the Doctor and I'm ";
	printer << 1500;
	printer << " years old";
	printer << endline;
	FileStream filePrinter = FileStream("3.txt");
	filePrinter << "I am the Doctor and I'm ";
	filePrinter << 1500;
	filePrinter << " years old";
	filePrinter << endline;
	OutStreamEncrypted en = OutStreamEncrypted(3);
	en << "I am the Doctor and I'm ";
	en << 1500;
	en << " years old";
	en << endline;
	Logger log = Logger();
	log << "I am the Doctor and I'm ";
	log << 1500;
	log << " years old";
	log << endline;
	Logger log2 = Logger();
	log2 << "I am the Doctor and I'm ";
	log2 << 1500;
	log2 << " years old";
	log2 << endline;
	return 0;
}
