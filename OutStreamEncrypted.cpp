#include "OutStreamEncrypted.h"
OutStreamEncrypted::OutStreamEncrypted(int hist)
{
	this->hist = hist;
}
OutStreamEncrypted::~OutStreamEncrypted()
{

}
OutStreamEncrypted& OutStreamEncrypted::operator << (const char *str)
{
	int i = 0;
	for (i = 0; str[i]; i++)
	{
		if (str[i] >= 32 && str[i] <= 126)
		{
			printf("%c",(str[i] + this->hist));
		}
		else
		{
			printf("%c", str[i]);
		}
	}
	return *this;
}
OutStreamEncrypted& OutStreamEncrypted::operator << (int num)
{
	printf("%d", num);
	return *this;
}
OutStreamEncrypted& OutStreamEncrypted::operator << (void(*pf)(FILE * outputFile))
{
	pf(stdout);
	return *this;
}