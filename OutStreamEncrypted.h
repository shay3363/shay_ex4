#pragma once
#include "OutStream.h"
class OutStreamEncrypted : public OutStream
{
private:
	int hist;
public:
	OutStreamEncrypted(int hist);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(void(*pf)(FILE * outputFile));
	OutStreamEncrypted& operator<<(int num);
};